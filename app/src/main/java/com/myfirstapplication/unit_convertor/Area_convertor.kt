package com.myfirstapplication.unit_convertor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_area_convertor.*

class Area_convertor : AppCompatActivity() {

    var from:Int=0
    var to:Int=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_area_convertor)


        var units= listOf<String>("square meters","square kilometers","square miles")
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item, units
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        sp_from.adapter=adapter
        sp_to.adapter=adapter


        sp_to.onItemSelectedListener= object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                to = id.toInt()
                Log.d("units",to.toString())



            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

        sp_from.onItemSelectedListener= object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                from =id.toInt()

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }}

        btn_convert.setOnClickListener{
            if(to==1 && from==0){//meter to km
                Log.d("units","converting meters to kilos")

//
                tv_result.text=(et_Input.text.toString().toDouble()/1000000).toString() //convert meter t0 kilos



            }else if(to==1 && from==2){//miles to kilos
                Log.d("units","converting miles to kilos")
                tv_result.text=(et_Input.text.toString().toDouble()*2.59
                        ).toString()
            }


            else if(to==2 && from==0){ //meter to miles
                Log.d("units","converting metres to miles")
                tv_result.text=(et_Input.text.toString().toDouble()/ 2590000).toString()
            }
            else if(to==2 && from==1){ //kilos to miles
                Log.d("units","converting kilos to miles")
                tv_result.text=(et_Input.text.toString().toDouble()/2.59
                        ).toString()
            }
            else if(to==0 && from==1){ //kilos to metres
                Log.d("units","converting kilos to meters")
                tv_result.text=(et_Input.text.toString().toDouble()*1000000).toString()
            }
            else if(to==0 && from==2){ //mile to metres
                Log.d("units","converting kilos to miles")
                tv_result.text=(et_Input.text.toString().toDouble()*2590000).toString()
            }
            else{
                tv_result.text= et_Input.text.toString() //the to == the from

            }
        }
    }
}
