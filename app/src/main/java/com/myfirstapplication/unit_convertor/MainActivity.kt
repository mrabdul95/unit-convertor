package com.myfirstapplication.unit_convertor

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        startActivity(Intent(this, Distance_convertor::class.java))

        btn_length.setOnClickListener {
            startActivity(Intent(this, Distance_convertor::class.java))
        }
        btn_speed.setOnClickListener {
            startActivity(Intent(this,Speed_convertor::class.java))
        }
        btn_temp.setOnClickListener {
            startActivity(Intent(this,Tempretaure_Convertor::class.java))
        }
        btn_area.setOnClickListener {
            startActivity(Intent(this,Area_convertor::class.java))
        }
        btn_currancy.setOnClickListener {
            startActivity(Intent(this, Currancy_Activity::class.java))
        }

    }
          }


