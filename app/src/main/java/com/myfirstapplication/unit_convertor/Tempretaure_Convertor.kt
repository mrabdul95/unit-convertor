package com.myfirstapplication.unit_convertor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_tempretaure__convertor.*

class Tempretaure_Convertor : AppCompatActivity() {
    var from:Int=0
    var to:Int=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tempretaure__convertor)

        var units= listOf<String>("Celsius","Fahrenheit","Kelvin")
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item, units
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        sp_from.adapter=adapter
        sp_to.adapter=adapter


        sp_to.onItemSelectedListener= object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                to = id.toInt()
                Log.d("units",to.toString())



            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

        sp_from.onItemSelectedListener= object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                from =id.toInt()

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }}

        btn_convert.setOnClickListener{
            if(to==1 && from==0){//cel to feh
                Log.d("units","converting meters to kilos")

//              //(0°C × 9/5) + 32
                tv_result.text=((et_Input.text.toString().toDouble()*1.8)+ 32).toString() //convert meter t0 kilos


                //(0K − 273.15) × 9/5 + 32

            }else if(to==1 && from==2){//kel to feh
                Log.d("units","converting miles to kilos")
                tv_result.text=((et_Input.text.toString().toDouble()-273.15)*1.8+32).toString()
            }


            //0°C + 273.15
            else if(to==2 && from==0){ //cel to kel
                Log.d("units","converting metres to miles")
                tv_result.text=(et_Input.text.toString().toDouble()+273.15 ).toString()
            }
            //(0°F − 32) × 5/9 + 273.15
            else if(to==2 && from==1){ //feh to kel
                Log.d("units","converting kilos to miles")
                tv_result.text=(((et_Input.text.toString().toDouble()-32.0)*0.55555555555
                        )+273.15).toString()
            }
            //(0°F − 32) × 5/9
            else if(to==0 && from==1){ //feh to cel
                Log.d("units","converting kilos to meters")
                tv_result.text=((et_Input.text.toString().toDouble()-32)*0.55555555555
                        ).toString()
            }

            else if(to==0 && from==2){ //kel to cel
                Log.d("units","converting kilos to miles")
                tv_result.text=(et_Input.text.toString().toDouble()-273.15).toString()
            }
            else{
                tv_result.text= et_Input.text.toString() //the to == the from

            }
        }
    }
}
